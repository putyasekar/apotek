<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Session;

class OrderSubmitController extends Controller
{
      public function index()
    {
        $Order = Order::orderBy('created_at','desc')->paginate(5);
        $Product = Product::all();

        return view('order.index', compact('Order','Product'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'product_id' => 'required',
            
            ]);

        try{
            $Order = new Order();
            $Order->product_id = $request->product_id;
           
            $Order->save();

            Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e) {
            Session::flash('message','Data Tidak Berhasil Disimpan');

            return redirect()->back();
        }

    }

     public function search_order(Request $request)
    {
    $Order = Order::query();
    $Product = Product::query();
    if(request()->has("search")&& strlen(request()->query("search"))>=1){
        $Order->where(
            "product_id","like","%". request()->query("search")."%");
         }

         //query pagination
         $pagination = 5;
         $Order = $Order->orderBy('created_at','desc')
         ->paginate($pagination);

         return view('order.index', compact('Order','Product'));

    }

     public function filter_order(Request $request)
    {

        $pagination = 5;
        $Product = Product::all();
        $Order = Order::whereDate('created_at','=', $request->created_at)->orderBy('created_at','desc')->paginate($pagination);

        return view('order.index', compact('Order','Product'));

    }

     public function delete($id)
    {
      $Order = Order::find($id);

      $Order->delete();

      Session::flash('message','Berhasil menghapus');

      return redirect()->back();
    }


    public function detail($id)
    {
      $Product = Product::all();
      $Order = Order::find($id);

      return view('order.detail', compact('Order','Product'));
    }

}
