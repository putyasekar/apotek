<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Session;

class ProductSubmitController extends Controller
{
    public function index()
    {
       $Product = Product::orderBy('created_at','desc')->paginate(5);
       $Category = Category::all();

       return view('product.index', compact('Product','Category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'category' => 'required',
            'price' => 'required',
            
            ]);

        try{
            $product = new Product();
            $product->name = $request->name;
            $product->category = $request->category;
            $product->price = $request->price;

            $product->save();

          Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e) {
            Session::flash('message','Data Tidak Berhasil Disimpan');

            return redirect()->back();
        }
    }

    //codingan buat search
    // 
    // {
    //     $pagination = 5;
    //     $Genre = Genre::all();
    //     $Produk = Produk::where('title','=', $request->title)->
    //     orderBy('created_at','desc') ->paginate($pagination);
    //     return view ('produk.produk', compact('Produk','Genre'));
    public function search_product(Request $request)
    {
    $Product = Product::query();
    $Category = Category::query();
    if(request()->has("search")&& strlen(request()->query("search"))>=1){
        $Product->where(
            "name","like","%". request()->query("search")."%");
         }

         //query pagination
         $pagination = 5;
         $Product = $Product->orderBy('created_at','desc')->paginate($pagination);

         return view('product.index', compact('Product','Category'));
    }

    public function edit($id)
    {
        $Category = Category::all();
        $Product = Product::find($id);

        return view('product.edit',compact('Product','Category'));
    }

    public function filter_product(Request $request)
    {

        $pagination = 5;
        // kenapa ada kategori soalnya kalo nampilin data produk harus ada data kategori
        $Category = Category::all();
        $Product = Product::whereDate('created_at','=', $request->created_at)->orderBy('created_at','desc')->paginate($pagination);

        return view('product.index', compact('Product','Category'));

    }

   public function update(Request $request, $id)
    {
        $Category = Category::all();
        $Product = Product::find($id);
        $Product->name = $request->name;
        $Product->price = $request->price;
        $Product->category = $request->category;
        
        $Product->save();

        Session::flash('message','Berhasil update');

        return redirect()->back();
    }

    public function delete($id)
    {
      $Product = Product::find($id);

      $Product->delete();

      Session::flash('message','Berhasil menghapus');

      return redirect()->back();
    }

    public function detail($id)
    {
      $Category = Category::all();
      $Product = Product::find($id);

      return view('product.detail', compact('Product','Category'));
    }
}
