<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Session;

class CategorySubmitController extends Controller
{
    public function index()
    {
        $Category = Category::orderBy('created_at','desc')->paginate(5);

        return view('category.index', compact('Category'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            
            ]);

        try{
            $category = new Category();
            $category->name = $request->name;
         
            $category->save();

           Session::flash('message', 'Data Berhasil Disimpan');
            return redirect()->back();
        } catch (Exception $e) {
            Session::flash('message','Data Tidak Berhasil Disimpan');

            return redirect()->back();
        }
    }

    public function search_category(Request $request)
    {
    $Category = Category::query();
    if(request()->has("search")&& strlen(request()->query("search"))>=1){
        $Category->where(
            "name","like","%". request()->query("search")."%");
         }

         //query pagination
         $pagination = 5;
         $Category = $Category->orderBy('created_at','desc')
         ->paginate($pagination);

         return view('category.index', compact('Category'));

    }

    public function edit($id)
    {
       
        $Category = Category::find($id);

        return view('category.edit',compact('Category'));
    }


    public function filter_category(Request $request)
    {

        $pagination = 5;
        $Category = Category::whereDate('created_at','=', $request->created_at)->orderBy('created_at','desc')->paginate($pagination);

        return view('category.index', compact('Category'));

    }

 public function update(Request $request, $id)
    {
        
        $Category = Category::find($id);
        $Category->name = $request->name;
        
        $Category->save();

        Session::flash('message','Berhasil update');

        return redirect()->back();
    }

     public function delete($id)
    {
      $Category = Category::find($id);

      $Category->delete();

      Session::flash('message','Berhasil menghapus');

      return redirect()->back();
    }

      public function detail($id)
    {
      $Category = Category::all();
      $Category = Category::find($id);

      return view('category.detail', compact('Category'));
    }
}