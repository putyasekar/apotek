<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryController extends Controller
{
    public function index()
    {
    try{
        $category = Category::all();

        $response = $category;
        $code = 200;
    } catch (Exception $e) {
        $code = 500;
        $response=$e->getMessage();
    }
    return apiResponseBuilder($code, $response);
}

public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            ]);

        try{
            $category = new Category();
            $category->name = $request->name;
         
            $category->save();

            $code = 200;
            $response = $category;
        }catch (\Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }
}