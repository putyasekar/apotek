<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
// use App\Models\Category;
// use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductController extends Controller
{
    public function index()
    {
        try{
            $product = Product::all();

            $response = $product;
            $code = 200;
        } catch (Exception $e) {
            $code=500;
            $response=$e->getMessage();
        }
        return apiResponseBuilder($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'category' => 'required',
            'price' => 'required',
            
            ]);

        try{
            $product = new Product();
            $product->name = $request->name;
            $product->category = $request->category;
            $product->price = $request->price;
            
            $product->save();

            $code = 200;
            $response = $product;
        }catch (\Exception $e){
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBuilder($code,$response);
    }
}