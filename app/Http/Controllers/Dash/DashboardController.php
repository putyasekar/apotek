<?php

namespace App\Http\Controllers\Dash;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;

class DashboardController extends Controller
{
    public function count()
    {
    	$Category = Category::all()->count();
    	$Order = Order::all()->count();
    	$Product = Product::all()->count();

    	return view('dash.index', compact('Category', 'Order', 'Product'));
    }
}