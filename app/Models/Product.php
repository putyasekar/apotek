<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function Category()
    {
    	return $this->belongsTo(Category::class, 'category_id','id');
    }
    public function Order()
    {
    	return $this->hasMany(Order::class, 'id','product_id');
    }
}
