<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    public function Product()
    {
    	return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
