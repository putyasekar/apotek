<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Putya',
        	'address' => 'Batam'
        ]);

        DB::table('users')->insert([
        	'name' => 'Diki',
        	'address' => 'Bogor'
        ]);
    }
}
