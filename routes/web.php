<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dash.index');
});

//category
Route::post('/category/store', 'Category\CategorySubmitController@store')->name('category.store');
Route::get('/category', 'Category\CategorySubmitController@index');
Route::get('/search_category', 'Category\CategorySubmitController@search_category')->name('search_category');
Route::get('/filter_category', 'Category\CategorySubmitController@filter_category')->name('filter_category');
Route::get('/category/{id}/edit','Category\CategorySubmitController@edit');
Route::post('/category/{id}/update','Category\CategorySubmitController@update');
Route::get('/category/{id}/delete','Category\CategorySubmitController@delete');
Route::get('/category/{id}', 'Category\CategorySubmitController@detail');

//order
Route::post('/order/store', 'Order\OrderSubmitController@store')->name('order.store');
Route::get('/order', 'Order\OrderSubmitController@index');
Route::get('/search_order', 'Order\OrderSubmitController@search_order')->name('search_order');
Route::get('/filter_order', 'Order\OrderSubmitController@filter_order')->name('filter_order');

//product
Route::post('/product/store', 'Product\ProductSubmitController@store')->name('product.store');
Route::get('/product', 'Product\ProductSubmitController@index');
Route::get('/search_product', 'Product\ProductSubmitController@search_product')->name('search_product');
Route::get('/filter_product', 'Product\ProductSubmitController@filter_product')->name('filter_product');

//dashboard
Route::get('/count', 'Dash\DashboardController@count')->name('count');
