<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//category
Route::apiResource('category', 'API\V1\CategoryController');

//order
Route::apiResource('order', 'API\V1\OrderController');

//product
Route::apiResource('product', 'API\V1\ProductController');

//register
Route::post('register', 'API\Auth\RegisterController@register');