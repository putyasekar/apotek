@include('base.header')

<div class="content-wrapper">
  <section class ="content-header">
    <h1>Product</h1>
  </section>

  <section class="content">
     <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Product</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif

              <form role="form" action="/products/{{$Products->id}}/update" method="post" enctype="multipart/form-data">
                @csrf

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>ID</label>
                  <input type="text" class ="form-control" name="id" value="{{ $Products->id }}" readonly="true">
                </div>
                 <div class="form-group">
                  <label>Product Name</label>
                  <input type="text" class="form-control" name="name" value="{{ $Products-> name }}">
                </div>

                 <div class="form-group">
                  <label>Price</label>
                  <input type="text" class="form-control" name="price" value="{{ $Products-> price }}">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Category</label>
                  <select class="form-control" name="category_id" required="">

                    @foreach($Category as $category)
                      <option value="{{ $category->id }}"{{($Products->category == $category->id)?'selected':''}}>{{ $category->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <input class="btn btn-primary" type="submit" value="update"></input>
                  <a class="btn btn-warning" href="/products">Back</a>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
</div>

@include('base.footer')