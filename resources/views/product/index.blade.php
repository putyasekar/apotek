@include('base.header')

<div class="content-wrapper">
	<section class="content-header">
		<h1>Product</h1>
	</section>
  
  <section class="content">
    <div class="box-body">
      <form action="{{ route('filter_product') }}" method="get" class="form-group">
        @csrf
        <label>PILIH TANGGAL</label>
        <input type="date" name="created-at">
        <input type="submit" name="FILTER">
      </form>
    </div>

		<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Insert Product Items</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form action="/product/store" method="POST">
                @csrf

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter The Name Product...">
                </div>
                <div class="form-group">
                  <label>Price</label>
                  <input type="text" class="form-control" name="category" placeholder="Enter The Price Product...">
                </div>
                <div class="form-group">
                  <label>Order</label> 
                  <input type="text" class="form-control" name="order" placeholder="Enter The Order Product...">
                </div>

                  <div class="form-group">
              <label for="exampleInputEmail">Category</label>
              <select class="form-control" name="category_id" required>
                <option> -- Pilih Salah Satu --</option>
                @foreach($Category as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
            </div>

                <input type="submit" name="Add">
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Product Table</h3>

              <div class="box-tools">
                 <form action="{{ route('search_product') }}" method="GET" class="form-group">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <form action="/product" method="GET">
              </form>
              <table class="table table-hover">
                <tr>
                  <th>id</th>
                  <th>name</th>
                  <th>price</th>
                  <th>category</th>
                   <th>order</th>
                </tr>

                @foreach($Product as $item)
                <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->name}}</td>
                  <td>{{$item->price}}</td>
                  <td>{{$item->category}}</td>
                  <td>{{$item->order}}</td>
                </tr>
                @endforeach
              </table>
            </div>

            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
	</section>
</div>

@include('base.footer')