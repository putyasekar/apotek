@include('base.header')

<div class="content-wrapper">
  <section class ="content-header">
    <h1>Category</h1>
  </section>

  <section class="content">
     <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Category</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif

              <form role="form" action="/category/{{$Category->id}}/update" method="post" enctype="multipart/form-data">
                @csrf

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>id</label>
                  <input type="text" class ="form-control" name="id" value="{{ $Category->id }}" readonly="true">
                </div>
                 <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control" name="name" value="{{ $Category-> name }}">
                </div>
                  </select>
                </div>
                <div class="form-group">
                  <input class="btn btn-primary" type="submit" value="update"></input>
                  <a class="btn btn-warning" href="/product">Back</a>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
</div>

@include('base.footer')