@include('base.header')

<div class="content-wrapper">
  <section class ="content-header">
    <h1>Category</h1>
  </section>

  <section class="content">

     <div class="box-body">
      <form action="{{ route('filter_category') }}" method="get" class="form-group">
        @csrf
        <label>PILIH TANGGAL</label>
        <input type="date" name="created_at">
        <input type="submit" value="FILTER">
      </form>
    </div>

     <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Category Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form action="/category/store" method="POST">
                @csrf

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control" name="name"
                  placeholder="ENTER CATEGORY ...">
                </div>
                <input type="submit" value="Add">
              </form>
            </div>
            <!-- /.box-body -->
          </div>

         <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Category Table</h3>

               <div class="box-tools">
                 <form action="{{ route('search_category') }}" method="GET" class="form-group">
                 <span class="pull-right">
                               <input type="text" name="search" class="form-control" placeholder="Search here...">
                           </span>
                 </form>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <from action="/category" method="GET">
              </from>
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Action</th>
                </tr>

                @foreach($Category as $item)
                <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->name}}</td>
                   <td>
                    <a class="btn btn-success btn-sm" href="/category/{{$item->id}}/edit">Update</a>
                    <a class="btn btn-danger btn-sm" href="/category/{{$item->id}}/delete">Delete</a>
                    <a class="btn btn-primary btn-sm" href="/category/{{$item->id}}">Detail</a>
                  </td>
                </tr>
                @endforeach
              </table>
              <!-- //supaya dia berpage2 ke samping -->
            </div>
            <div class="text-center">
              {{$Category->links()}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
  </section>
</div>

@include('base.footer')