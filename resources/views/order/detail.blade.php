@include('base.header')

<div class="content-wrapper">
  <section class ="content-header">
    <h1></h1>
  </section>

  <section class="content">
     <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Blade</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif

              <form role="form" action="/order/{{$Order->id}}/update" method="post" enctype="multipart/form-data">
                @csrf

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
                <table class="table table-bordered">
                  <tr>
                    <td>Id</td>
                    <td>{{ $Order->id }}</td>
                  </tr>
                  <tr>
                    <td>Product ID</td>
                    <td>{{ $Order->product_id }}</td>
                  </tr>
                </table>
                <div class="form-group">
                  <!-- <input class="btn btn-primary" type="submit" value="update"></input> -->
                  <a class="btn btn-warning" href="/order">Back</a>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

  </section>
</div>

@include('base.footer')