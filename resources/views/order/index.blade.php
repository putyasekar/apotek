@include('base.header')

<div class="content-wrapper">
  <section class ="content-header">
    <h1>Order</h1>
  </section>

  <section class="content">

     <div class="box-body">
      <form action="{{ route('filter_order') }}" method="get" class="form-group">
        @csrf
        <label>PILIH TANGGAL</label>
        <input type="date" name="created_at">
        <input type="submit" value="FILTER">
      </form>
    </div>

     <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Add Order</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(Session::has('message'))
              <h4><strong>{{session::get('message')}}</strong></h4>
              @endif
              <form action="/order/store" method="POST">
                @csrf

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <!-- text input -->
               
                <div class="form-group">
                  <label for="exampleInputEmail1">Product Name</label>
                  <select class="form-control" name="product_id" required="" >
                    @foreach( $Product as $product)
                      <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                  </select>
                </div>

                
                <input type="submit" value="Add">
              </form>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Order Table</h3>

              <div class="box-tools">
                 <form action="{{ route('search_order') }}" method="GET" class="form-group">
                 <span class="pull-right">
                               <input type="text" name="search" class="form-control" placeholder="Search here...">
                           </span>
                 </form>
               </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <from action="/order" method="GET">
              </from>
              <table class="table table-hover">
                <tr>
                  <th>ID</th>
                  <th>Order Id</th>
                  <th>Action</th>
                
                </tr>

                @foreach($Order as $item)
                <tr>
                  <td>{{$item->id}}</td>
                  <td>{{$item->product_id}}</td>

                  <td>
                    <a class="btn btn-success btn-sm" href="/order/{{$item->id}}/edit">Update</a>
                    <a class="btn btn-danger btn-sm" href="/order/{{$item->id}}/delete">Delete</a>
                    <a class="btn btn-primary btn-sm" href="/order/{{$item->id}}">Detail</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>

             <!-- //supaya dia berpage2 ke samping -->
            </div>
            <div class="text-center">
              {{$Order->links()}}
            </div>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
  </section>
</div>

@include('base.footer')